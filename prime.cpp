﻿#include <iostream>

bool isPrime(int number) {
    for (int i = 2; i <= sqrt(number); i++) {
        if (number % i == 0) {
            return false;
        }
    }
    return true;
}

int main() {
    if (isPrime(17)) {
        std::cout <<"17 is a prime number" << std::endl;
    }
    else {
        std::cout << "17 is a composite number" << std::endl;
    }

    return 0;
}

