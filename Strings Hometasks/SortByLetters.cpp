#include <iostream>

using namespace std;

bool isLettersMatch(const string& word) {
    if (word.length() > 1 && tolower(word[0]) == tolower(word[word.length() - 1])) {
        return true;
    }
    return false;
}

void findWordsWithMatchingLetters(const string& text) {
    string word;
    string delimiter = " \t\n\r\f\v.,:;?!";

    size_t startPos = 0;
    size_t endPos = text.find_first_of(delimiter, startPos);

    while (endPos != string::npos) {
        word = text.substr(startPos, endPos - startPos);

        if (!word.empty() && isLettersMatch(word)) {
            cout << word << endl;
        }


        startPos = text.find_first_not_of(delimiter, endPos);
        endPos = text.find_first_of(delimiter, startPos);
    }

    word = text.substr(startPos);


    if (!word.empty() && isLettersMatch(word)) {
        cout << word << endl;
    }
}

    int main() {
        string text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
        findWordsWithMatchingLetters(text);

        return 0;
    }