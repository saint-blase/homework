#include <iostream>

using namespace std;

bool isAnnagram(const char* word1, const char* word2)
{
    int count1[256] = { 0 };
    int count2[256] = { 0 };

    for (int i = 0; word1[i] != '\0'; i++) {
        count1[word1[i]]++;
    }

    for (int i = 0; word2[i] != '\0'; i++) {
        count2[word2[i]]++;
    }

    for (int i = 0; i < 256; i++) {
        if (count1[i] != count2[i]) {
            return false;
        }
    }
    return true;
}

int main()
{
    const char* word1 = "star";
    const char* word2 = "home";

    if (isAnnagram(word1, word2)) {
        cout << word1 << " and " << word2 << " are annagrams" << endl;
    }
    else {
        cout << word1 << " and " << word2 << " are not annagrams" << endl;
    }
    return 0;
}


