﻿#include<iostream>
using namespace std;

void reverseArray(int arr[], int n)
{
    for (int i = 0; i < n / 2; i++) 
    {
        int temp = arr[i];
        arr[i] = arr[n - 1 - i];
        arr[n - 1 - i] = temp;
    }
}

int main() 
{
    int arr[] = { 6, 2, 3, 8, 1 };
    int n = sizeof(arr) / sizeof(arr[0]);

    cout << "old numbers: ";

    for (int i = 0; i < n; i++) 
    {
        cout << arr[i] << " ";
    }

    reverseArray(arr, n);

    cout << "\nnew numbers: ";

    for (int i = 0; i < n; i++) 
    {
        cout << arr[i] << " ";
    }
   return 0;
}
