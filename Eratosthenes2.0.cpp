﻿#include <iostream>

using namespace std;

void createMap(bool arr[], int n);
void output(bool arr[], int n);

int main()
{
    const int n = 100;
    bool map[n] = { 0 };
    createMap(map, n);
    output(map, n);
}

void createMap(bool arr[], int n)
{
    arr[0] = true;

    for (int i = 1; i < sqrt(n); i++)
    {
        if (!arr[i])
        {
            for (int j = 2 * (i + 1); j <= n; j += i + 1)
            {
                arr[j - 1] = true;
            }
        }
    }
}

void output(bool arr[], int n)
{
    int a[100];
    for (int i = 0; i < n; i++)
    {
        if (!arr[i])
        {
            cout << (i + 1) << ' '; arr[i] == 0 >= i + 1;
        }
    }
}