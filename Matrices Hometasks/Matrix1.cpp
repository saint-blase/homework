#include <iostream>
#include <cstdlib>

using namespace std;

int** createSymmetricMatrix(int size) 
{
    int** matrix = new int* [size];

    for (int i = 0; i < size; i++)
    {
        matrix[i] = new int[size];

        for (int j = 0;j < size; j++) 
        {
            if (i >= j)
            {
                int randomValue = rand();
                matrix[i][j] = randomValue;
                matrix[j][i] = randomValue;
            }
        }
    }
    return matrix;

}

int** addMatrices(int** matrix1, int** matrix2, int size)
{
    int** result = new int* [size];

    for (int i = 0; i < size; i++)
    {
        result[i] = new int[size];
        for (int j = 0; j < size; j++)
        {
            result[i][j] = matrix1[i][j] + matrix2[i][j];
        }
    }

    return result;
}

void printMatrix(int** matrix, int size)
{
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }
}


int main()
{
    int size = 4; 

    int** matrix1 = createSymmetricMatrix(size); 
    int** matrix2 = createSymmetricMatrix(size); 

    int** sumMatrix = addMatrices(matrix1, matrix2, size); 

    cout << "Sum of matrices:" << endl;
    printMatrix(sumMatrix, size);

    return 0;
}

