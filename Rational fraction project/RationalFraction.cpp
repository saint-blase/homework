#include "RationalFraction.h"
#include <iostream>

using namespace std;

RationalFraction::RationalFraction(int num, int denom) {
	numerator = num;
	denominator = denom;

	if (denom == 0) {
		throw std::invalid_argument("Denominator can not be equal to zero");
	}
}

void RationalFraction::print() {
	cout << numerator << "/" << denominator << endl;
}

RationalFraction RationalFraction::add(RationalFraction other) {
	int newNumerator = numerator * other.denominator + other.numerator * denominator;
	int newDenominator = denominator * other.denominator;
	return RationalFraction(newNumerator, newDenominator);
}

RationalFraction RationalFraction::subtract(RationalFraction other) {
	int newNumerator = numerator * other.denominator - other.numerator * denominator;
	int newDenominator = denominator * other.denominator;
	return RationalFraction(newNumerator, newDenominator);
}

RationalFraction RationalFraction::multiply(RationalFraction other) {
	int newNumerator = numerator * other.numerator;
	int newDenominator = denominator * other.denominator;
	return RationalFraction(newNumerator, newDenominator);
}

RationalFraction RationalFraction::divide(RationalFraction other) {
	if (other.numerator == 0) {
		throw std::invalid_argument("Divisor can not be equal to zero");
	}

	int newNumerator = numerator * other.denominator;
	int newDenominator = denominator * other.numerator;
	return RationalFraction(newNumerator, newDenominator);
}