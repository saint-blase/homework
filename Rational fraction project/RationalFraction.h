#pragma once

class RationalFraction
{
private:
	int numerator;
	int denominator;

public:
	RationalFraction(int num, int denom);
	void print();
	RationalFraction add(RationalFraction other);
	RationalFraction subtract(RationalFraction other);
	RationalFraction multiply(RationalFraction other);
	RationalFraction divide(RationalFraction other);
};

