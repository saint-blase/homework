#include <iostream>
#include "RationalFraction.h"

using namespace std;

int main()
{
    try {
        RationalFraction term1(7, 8);
        RationalFraction term2(12, 0);

        cout << "Fraction 1: ";
        term1.print();

        cout << "Fraction 2: ";
        term2.print();

        RationalFraction sum = term1.add(term2);
        cout << "Sum: ";
        sum.print();

        RationalFraction difference = term1.subtract(term2);
        cout << "Difference: ";
        difference.print();

        RationalFraction product = term1.multiply(term2);
        cout << "Product: ";
        product.print();

        RationalFraction quotient = term1.divide(term2);
        cout << "quotient: ";
        quotient.print();

    }
    catch (const std::invalid_argument& e) {
        std::cout << "Exception caught: " << e.what() << std::endl;
    }


    return 0;
}