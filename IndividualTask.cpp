#include <iostream>
using namespace std;

int main()
{
	int size;
	cout << "Enter amounts of elements in array:: ";
	cin >> size;

	int* arr = new int[size];

	cout << "Enter array elements: ";
	for (int i = 0; i < size; i++) {
		cin >> arr[i];
	}

	int* newArr = new int[size];
	int newSize = 0;

	for (int i = 0; i < size; i++) {
		if (i % 2 != 0 && arr[i] % 2 == 0) {
			newArr[newSize] = arr[i];
			arr[i] = 0;
		}
	}


	for (int i = 0; i < size - 1; i++) {
		for (int j = 0; j < size - 1; j++) {
			if (arr[j] < arr[j + 1]) {
				int temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}
	cout << "New array: ";
	for (int i = 0; i < newSize; i++) {
		cout << newArr[i] << " ";
	}

	cout << endl;

	cout << "Remaining sorted array: ";
	for (int i = 0; i < size; i++) {
		if (arr[i] != 0) {
			cout << arr[i] << " ";
		}
	}

	cout << endl;

	delete[]arr;
	delete[]newArr;

	return 0;

}
