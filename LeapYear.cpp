﻿#include <iostream>

bool isLeapYear(int year) {
    if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
        return true;
    }
    else {
        return false;
    }
}

int main() {
    int year;
    std::cout << "year: ";
    std::cin >> year;

    if (isLeapYear(year)) {
        std::cout << "Leap Year. 366 days." << std::endl;
    }
    else {
        std::cout << "Defolt year. 365 days." << std::endl;
    }

    return 0;
}
