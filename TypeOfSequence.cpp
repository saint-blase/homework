﻿#include <iostream> 
void typeOfSequence(int number);

int main()
{
    int number;
    std::cin >> number;
    typeOfSequence(number);
}


void typeOfSequence(int number)
{
    number = number < 0 ? abs(number) : number;
    bool lessThan = false;
    bool moreThan = false;
    bool equals = false;
    int temp = number;
    int next = number % 10;
    number /= 10;
    while (number)
    {

        int previous = number % 10;
        if (next < previous)
        {
            moreThan = true;
        }
        else if (previous == next)
        {
            equals = true;
        }
        else
        {
            lessThan = true;
        }
        next = previous;
        number /= 10;
    }


    if (equals && !moreThan && !lessThan)
    {
        std::cout << temp << "Monotonic" << std::endl;
    }


    else if (equals && moreThan && !lessThan)
    {
        std::cout << temp << "Decreasing" << std::endl;
    }
    else if (!equals && moreThan && !lessThan)
    {
        std::cout << temp << "Strictly Decreasing" << std::endl;
    }
    else if (equals && !moreThan && lessThan)
    {
        std::cout << temp << "Increasing" << std::endl;
    }
    else if (!equals && !moreThan && lessThan)
    {
        std::cout << temp << "Strictly Increasing" << std::endl;
    }
    else if (!equals && !moreThan && !lessThan)
    {
        std::cout << temp << "one digit" << std::endl;
    }


    else
    {
        std::cout << temp << "Unsorted" << std::endl;
    }


}

