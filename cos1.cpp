#include <iostream>

using namespace std;

//cos1

int calculateCos () {
    double x, y, z, w;

    cout << "x: ";
    cin >> x;
    cout << "y: ";
    cin >> y;
    cout << "z: ";
    cin >> z;

    w = cos((z * z) + (x * x) / 4 + y);

    cout << "w: " << w << endl;

    return 0;
}

//cos2

int  computeCos() {
    double x, y, z, w;

    cout << "x: ";
    cin >> x;
    cout << "y: ";
    cin >> y;
    cout << "z: ";
    cin >> z;

    w = y + x - ((pow(cos(x),2)) / (1 + (sqrt(fabs(y+z)))));

    cout << "w: " << w << endl;

    return 0;
}

//sin1

int calculateSin() {
    double x, y, z, w;

    cout << "x: ";
    cin >> x;
    cout << "y: ";
    cin >> y;
    cout << "z: ";
    cin >> z;

    w = sin(fabs((y - sqrt(fabs(x))) * (x - y / (z * z) + (x * x) / 4)));

    cout << "w: " << w << endl;

    return 0;
}

//sin2

int computeSin() {
    double x, y, z, w;

    cout << "x: ";
    cin >> x;
    cout << "y: ";
    cin >> y;
    cout << "z: ";
    cin >> z;

    w = x - ((x * x) / (1 + pow(sin(x + y + z), 2)));

    cout << "w: " << w << endl;

    return 0;
}






